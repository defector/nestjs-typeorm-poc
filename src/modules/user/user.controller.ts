import { Body, Controller, Get, Inject, Param, Post } from "@nestjs/common";
import { USER_SERVICE } from "./constants";
import { IUserService } from "./contracts";
import { RegisterUserDto, UserDto } from "./dto";

@Controller("uts/transactions")
export class UserController {
    private userServive: IUserService;

    constructor(@Inject(USER_SERVICE) userServive: IUserService) {
        this.userServive = userServive;
    }

    @Post()
    public async createTransaction(
        @Body() user: RegisterUserDto
    ): Promise<UserDto> {
        const createdUser = await this.userServive.createTransaction(user);
        return createdUser;
    }

    // @Get()
    // public async getAllUsers(): Promise<UserDto[]> {
    //     const users = await this.userServive.getAllUsers();
    //     return users;
    // }

    @Get("/:txnId")
    public async getTransactionById(
        @Param("txnId") txnId: any
    ): Promise<UserDto> {
        const user = await this.userServive.getTransactionById(txnId);
        return user;
    }

    @Get("/:txnId/pricing-options")
    public async initiateTransaction(@Param("txnId") txnId: any): Promise<any> {
        const user = await this.userServive.initiateTransaction(txnId);
        return user;
    }

    @Post("/:txnId/evaluate")
    public async completeTransaction(@Param("txnId") txnId: any): Promise<any> {
        const user = await this.userServive.completeTransaction(txnId);
        return user;
    }
}
