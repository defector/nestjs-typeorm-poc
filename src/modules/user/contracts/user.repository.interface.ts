import { RegisterUserDto, UserDto } from "../dto";

export interface IUserRepository {
    findAll(): Promise<any>;
    findUserByUsername(username: string): Promise<UserDto | undefined>;
    findUserByEmail(email: string): Promise<UserDto | undefined>;
    findUserById(id: string): Promise<UserDto | undefined>;
    createUser(user: RegisterUserDto): Promise<UserDto>;
    updateUser(user: any): Promise<UserDto>;
}
