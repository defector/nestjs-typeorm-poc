import { RegisterUserDto, UserDto } from "../dto";

export interface IUserService {
    createTransaction(requestBody: any): Promise<any>;
    getTransactionById(txnId: any): Promise<any>;
    completeTransaction(txnId: any): Promise<any>;
    initiateTransaction(txnId: any): Promise<any>;
}
