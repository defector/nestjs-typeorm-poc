import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";

import { plainToClass } from "class-transformer";
import { Repository } from "typeorm";
import { QueryResultCache } from "typeorm/cache/QueryResultCache";

import { IUserRepository } from "../contracts";
import { RegisterUserDto, UserDto } from "../dto";
import { User } from "../entities";

@Injectable()
export class UserRepository implements IUserRepository {
    private userRepository: Repository<User>;

    constructor(@InjectRepository(User) userRepository: Repository<User>) {
        this.userRepository = userRepository;
    }

    public async findAll() {
        const userEntity = await this.userRepository.find({
            cache: true,
        });
        return userEntity;
    }

    public async findUserByUsername(
        username: string
    ): Promise<UserDto | undefined> {
        const userEntity = await this.userRepository.findOne({
            username: username,
        });
        const user = plainToClass(UserDto, userEntity);

        return user;
    }

    public async findUserByEmail(email: string): Promise<UserDto | undefined> {
        const userEntity = await this.userRepository.findOne({ email: email });
        const user = plainToClass(UserDto, userEntity);

        return user;
    }

    public async findUserById(id: string): Promise<UserDto | undefined> {
        const userEntity = await this.userRepository.find({
            where: { id: id },
            cache: {
              id: id,
              milliseconds: 25000,
            }
        });
        const user = plainToClass(UserDto, userEntity[0]);
        return user;
    }

    public async createUser(user: RegisterUserDto): Promise<UserDto> {
        const userEntity = this.userRepository.create(user);
        const createdUserEntity = await this.userRepository.save(userEntity);
        const createdUser = plainToClass(UserDto, createdUserEntity);

        return createdUser;
    }

    public async updateUser(user: any): Promise<any> {
        const id= user.id;
        const queryRunner = await this.userRepository.createQueryBuilder("User");
        const [sql, parameters] = await queryRunner.where("user.id = :id", { id: id }).getQueryAndParameters();
        const queryId = sql + " -- PARAMETERS: " + JSON.stringify(parameters);
        const result: any = {};
        Object.keys(user).forEach((key) => {
            result[`User_${key}`] = user[key];
        });
        const saveQueryCacheResult = {
          identifier: id,
          query: queryId,
          time: new Date().getTime(),
          duration:25000,
          result: JSON.stringify([result]),
        }
        const queryResultCache: any = this.userRepository.manager.connection.queryResultCache;
        try{
          await queryResultCache.storeInCache(saveQueryCacheResult, {
            identifier: id
          });
        }catch(err){
          console.log("Unable to update Cache Operations")
          try {
            await queryResultCache.remove([id])
          } catch (error) {
            console.log("Invalid Cache Operations")
          }
        }
        const userEntity = await this.userRepository.update(
            { id: user.id },
            user
        );
        return userEntity;
    }
}
