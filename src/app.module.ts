import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UserModule } from "./modules/user";

const databaseConfig = CONFIG.get("database");
const typeOrmConfig = Object.assign(databaseConfig, {
    entities: [__dirname + "/**/*.entity{.ts,.js}"],
    synchronize: true,
    cache: {
        type: "ioredis",
        options: {
            host: "localhost",
            port: 6379,
        },
        duration: 30000, // 30 seconds
    },
});

@Module({
    imports: [TypeOrmModule.forRoot(typeOrmConfig), UserModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
